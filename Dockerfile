FROM vagrantlibvirt/vagrant-libvirt:latest

RUN apt-get update \
  && apt-get install -y nfs-kernel-server sudo \
  && echo "%libvirt     ALL=(ALL)	NOPASSWD: ALL" >> /etc/sudoers \
  && rm -rf /var/lib/apt/lists/*